if (document.getElementsByClassName == undefined) {
	document.getElementsByClassName = function (cl) {
		var retnode = [];
		var myclass = new RegExp('\\b' + cl + '\\b');
		var elem = this.getElementsByTagName('*');
		for (var i = 0; i < elem.length; i++) {
			var classes = elem[i].className;
			if (myclass.test(classes)) {
				retnode.push(elem[i]);
			}
		}
		return retnode;
	}
}
(function (d) {
	function i() {
		var b = d("script:first"),
		a = b.css("color"),
		c = false;
		if (/^rgba/.test(a))
			c = true;
		else
			try {
				c = a != b.css("color", "rgba(0, 0, 0, 0.5)").css("color");
				b.css("color", a)
			} catch (e) {}

		return c
	}
	function g(b, a, c) {
		var e = "rgb" + (d.support.rgba ? "a" : "") + "(" + parseInt(b[0] + c * (a[0] - b[0]), 10) + "," + parseInt(b[1] + c * (a[1] - b[1]), 10) + "," + parseInt(b[2] + c * (a[2] - b[2]), 10);
		if (d.support.rgba)
			e += "," + (b && a ? parseFloat(b[3] + c * (a[3] - b[3])) : 1);
		e += ")";
		return e
	}
	function f(b) {
		var a,
		c;
		if (a = /#([0-9a-fA-F]{2})([0-9a-fA-F]{2})([0-9a-fA-F]{2})/.exec(b))
			c = [parseInt(a[1], 16), parseInt(a[2], 16), parseInt(a[3], 16), 1];
		else if (a = /#([0-9a-fA-F])([0-9a-fA-F])([0-9a-fA-F])/.exec(b))
			c = [parseInt(a[1], 16) * 17, parseInt(a[2], 16) * 17, parseInt(a[3], 16) * 17, 1];
		else if (a = /rgb\(\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*\)/.exec(b))
			c = [parseInt(a[1]), parseInt(a[2]), parseInt(a[3]), 1];
		else if (a = /rgba\(\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*,\s*([0-9\.]*)\s*\)/.exec(b))
			c = [parseInt(a[1], 10), parseInt(a[2], 10), parseInt(a[3], 10), parseFloat(a[4])];
		return c
	}
	d.extend(true, d, {
		support : {
			rgba : i()
		}
	});
	var h = ["color", "backgroundColor", "borderBottomColor", "borderLeftColor", "borderRightColor", "borderTopColor", "outlineColor"];
	d.each(h, function (b, a) {
		d.fx.step[a] = function (c) {
			if (!c.init) {
				c.a = f(d(c.elem).css(a));
				c.end = f(c.end);
				c.init = true
			}
			c.elem.style[a] = g(c.a, c.end, c.pos)
		}
	});
	d.fx.step.borderColor = function (b) {
		if (!b.init)
			b.end = f(b.end);
		var a = h.slice(2, 6);
		d.each(a, function (c, e) {
			b.init || (b[e] = {
					a : f(d(b.elem).css(e))
				});
			b.elem.style[e] = g(b[e].a, b.end, b.pos)
		});
		b.init = true
	}
})(jQuery);
(function () {
	var showPhotosObj;
	window.albumHide = function () {
		showPhotosObj.hide();
	}
	window.albumForward = function () {
		showPhotosObj.forward();
	}
	window.albumBackward = function () {
		showPhotosObj.backward();
	}
	window.ShowPhotos = function (imgArr, carrier) {
		var self = this;
		var carrier = document.getElementById(carrier);
		carrier.onclick = function (e) {
			e = e || event;
			var target = e.target || e.srcElement;
			if (target.className != "preview")
				return;
			var t = target.getAttribute('data-img');
			if (!t)
				t = target.src;
			var n = find(imgArr, t);
			self.start(n);
			function find(array, value) {
				for (var i = 0; i < array.length; i++) {
					if (array[i].src === value)
						return i;
				}
				return -1;
			}
		}
		var loop = 1;
		var controlPos = "left_right";
		var forwardBut = "/_res/img/arrow_right.png";
		var backwardBut = "/_res/img/arrow_left.png";
		var backText = "";
		var backBut = "/_res/img/closeButton.png";
		var forwardText = "";
		var backwardText = "";
		var coverBackgroundColor = "#333";
		var aColor = "#fff";
		var coverOpacity = .9;
		var num;
		var imgEl,
		coverEl,
		carrierEl;
		this.start = function (n) {
			showPhotosObj = self;
			num = n;
			if (imgArr.length > 0) {
				document.body.style.overflow = "hidden";
				showCarrier2();
			}
		}
		function showCarrier2() {
			coverEl = document.createElement("div");
			coverEl.style.width = "100%";
			coverEl.style.height = "100%";
			coverEl.style.position = "fixed";
			coverEl.style.top = "0px";
			coverEl.style.left = "0px";
			coverEl.style.zIndex = 19;
			coverEl.style.backgroundColor = coverBackgroundColor;
			coverEl.style.opacity = coverOpacity;
			coverEl.style.filter = "progid:DXImageTransform.Microsoft.Alpha(opacity=" + coverOpacity * 100 + ")";
			document.body.appendChild(coverEl);
			carrierEl = document.createElement("div");
			carrierEl.style.width = "100%";
			carrierEl.style.height = "100%";
			carrierEl.style.position = "fixed";
			carrierEl.style.top = "0px";
			carrierEl.style.left = "0px";
			carrierEl.style.textAlign = "center";
			carrierEl.style.zIndex = 100;
			carrierEl.innerHTML = "<div id = 'closeGallery' style = 'position:fixed;top:20px;right:20px;width:50px;height:50px;z-index:200;cursor:pointer;background-image:url(/_components/folderGallery/close.png);background-position:center;background-repeat:no-repeat;'></div><table style = 'width:100%;height:100%;'><tr>" + "<td style = 'width:20%;cursor:pointer;background-image:url(/_components/folderGallery/prev.png);background-position:center;background-repeat:no-repeat;' id = 'prevCarrier'></td>" + "<td id = 'imgCarrier' style = 'text-align:center;width:60%;'><img src = '/_sys/folderGallery/loading.gif' alt = 'загрузка' id = 'galleryImage'></td>" + "<td style = 'width:20%;background-image:url(/_components/folderGallery/next.png);background-position:center;background-repeat:no-repeat;cursor:pointer;' id = 'nextCarrier'></td>" + "</tr></table>";
			document.body.appendChild(carrierEl);
			imgEl = document.getElementById('galleryImage');
			imgEl.style.margin = "auto";
			imgEl.style.cursor = "pointer";
			imgEl.onclick = self.forward;
			imgEl.style.display = "block";
			imgEl.style.position = "relative";
			imgEl.ondblclick = function () {
				return false;
			}
			imgEl.onselectstart = function () {
				return false;
			}
			imgEl.onmousedown = function () {
				return false;
			}
			imgEl.oncontexmenu = function () {
				return false;
			}
			var next = document.getElementById("nextCarrier");
			next.onclick = function () {
				self.forward();
			}
			next.onmouseover = function () {}
			next.onmouseout = function () {
				next.style.backgroundColor = "";
			}
			var prev = document.getElementById("prevCarrier");
			prev.onclick = function () {
				self.backward();
			}
			prev.onmouseover = function () {}
			prev.onmouseout = function () {
				prev.style.backgroundColor = "";
			}
			var close = document.getElementById('closeGallery');
			close.onclick = function () {
				self.hide();
			}
			self.show();
		}
		this.show = function () {
			if (window.jQuery) {
				$(imgEl).fadeOut("fast");
			}
			resizeImg();
			imgEl.src = imgArr[num].src;
			imgEl.style.display = "none";
			if (window.jQuery) {
				$(imgEl).fadeIn("fast");
			}
			if (num < imgArr.length - 1)
				preload(num + 1);
		}
		this.forward = function () {
			num++;
			if (num >= imgArr.length) {
				loop == 1 ? num = 0 : self.hide();
				return;
			}
			self.show();
		}
		this.backward = function () {
			num--;
			if (num < 0) {
				loop == 1 ? num = imgArr.length - 1 : self.hide();
				return;
			}
			self.show();
		}
		this.hide = function () {
			clearNode(carrierEl);
			clearNode(coverEl);
			document.body.removeChild(carrierEl);
			document.body.removeChild(coverEl);
			document.body.style.overflow = "auto";
		}
		function clearNode(node) {
			while (node.hasChildNodes()) {
				var firstChild = node.firstChild;
				node.removeChild(node.firstChild);
			}
		}
		function preload(n) {
			var tpImg = new Image();
			tpImg.src = imgArr[n].src;
		}
		function getClientHeight() {
			return window.innerHeight;
		}
		function getClientWidth() {
			return document.documentElement.clientWidth;
		}
		function resizeImg() {
			if (!imgArr[num])
				return;
			if (getClientHeight() >= imgArr[num].height) {
				imgEl.style.height = imgArr[num].height + "px";
				imgEl.style.width = "";
				return;
			} else {
				imgEl.style.height = getClientHeight() + "px";
				imgEl.style.width = "";
				return;
			}
			if (getClientWidth() <= imgArr[num].width + 0.4 * getClientWidth()) {
				imgEl.style.width = getClientWidth() - 0.4 * getClientWidth() + "px";
				imgEl.style.height = "";
				return;
			}
			imgEl.style.height = getClientHeight() + "px";
			imgEl.style.width = "";
		}
		window.onresize = function () {
			if (imgArr)
				resizeImg();
		}
	}
})();
function substr(f_string, f_start, f_length) {
	if (f_start < 0) {
		f_start += f_string.length;
	}
	if (f_length == undefined) {
		f_length = f_string.length;
	} else if (f_length < 0) {
		f_length += f_string.length;
	} else {
		f_length += f_start;
	}
	if (f_length < f_start) {
		f_length = f_start;
	}
	return f_string.substring(f_start, f_length);
}
