$(document).ready( function() {
	$('#tabContainer').easytabs({tabs: "#mainBlockMenu > ul > li"});
	$('#priceTabContainer').easytabs({tabs: "#priceBlockMenu > ul > li"});
});


$(window).load(function() {
	$('#slider').nivoSlider({
		effect: 'sliceUpLeft', // Specify sets like: 'fold,fade,sliceDown'
		animSpeed: 1000, // Slide transition speed
		pauseTime: 600000, // How long each slide will show
		startSlide: 0, // Set starting Slide (0 index)
		directionNav: true, // Next & Prev navigation
		directionNavHide: false, // Only show on hover
		controlNav: true, // 1,2,3... navigation
		controlNavThumbs: false, // Use thumbnails for Control Nav
		controlNavThumbsFromRel: true, // Use image rel for thumbs
		controlNavThumbsSearch: '.jpg', // Replace this with...
		controlNavThumbsReplace: '_thumb.jpg', // ...this in thumb Image src
		keyboardNav: true, // Use left & right arrows
		pauseOnHover: true, // Stop animation while hovering
		manualAdvance: false, // Force manual transitions
		captionOpacity: 0.8, // Universal caption opacity
		afterChange: function (){window.modalOrderHandler();window.setMask();window.setClientName();window.setSendButton();},
		prevText: '', // Prev directionNav text
		nextText: '' // Next directionNav text
	});
	
	$(window).resize();
	window.setMask();
	window.setClientName();
	window.modalOrderHandler();
	window.setSendButton();
});

//Caption
(function() {
	$(document).ready( function() {
		$(window).resize(function(){
			$('.nivo-caption').css({
				position:'absolute',
				left: ($(window).width() - $('.nivo-caption').outerWidth())/2
			});
		});
	});
})();

//Поля ввода
(function() {
	window.setMask = function() {
		$(document).ready( function() {
			$(".clientPhone").mask("+7 (999) 999-99-99");
		});
	}

	window.setMask();
})();

//Поля ввода имени
(function() {
	window.setClientName = function() {
		$(document).ready( function() {
			$(".clientName").focusout(function() {
				if (this.value == "") this.value = "Ваше имя";
			});
			$(".clientName").focus(function() {
				this.value = "";
			});
		});
	}

	window.setClientName();
})();

//ПРОКРУТКА ДЛЯ ССЫЛОК МЕНЮ
(function() {
	$(".scroller").mPageScroll2id({offset:45});
})();

//---------------//
//ПРОКРУТКА
(function() {
	window.onscroll = window.onload = function() {
		var scrolled = window.pageYOffset || document.documentElement.scrollTop;
		var clientHeight = document.documentElement.clientHeight;
		
		if (scrolled > 120) {
			var el = document.getElementById('mainMenuCarrier');
			el.style.position = "fixed";
			el.style.top = "0px";
			document.getElementById("topAdvantages").style.marginTop = 37 + el.offsetHeight + "px";
		} else {
			var el = document.getElementById('mainMenuCarrier');
			el.style.position = "static";
			el.style.top = "";
			document.getElementById("topAdvantages").style.marginTop = "0px";
		}
		
		
		var upEl = document.getElementById('upButton');
		upEl.onclick = function() {
			if (window.jQuery) {
				$(document).ready(function() {
					$('html, body').stop().animate({scrollLeft: 0, scrollTop:$("body").offset().top}, 500);
				});
			} else {
				window.scrollTo(0,0);
			}
		}

		if (scrolled > 100) {
			upEl.style.display = "block";
		} else {
			upEl.style.display = "none";
		}
	}
})();

//---------------//
//ОТПРАВКА СООБЩЕНИЙ
(function() {
	window.sendMail = function(target) {
		//alert(target);
		var fieldsInfo = { //обязательные поля
			clientName: {
				required: 1,
				minLength: 2,
				defaultVal: "Ваше имя",
				error: "Пожалуйста, введите Ваше имя"
			},
			clientPhone: {
				required: 1,
				minLength:11,
				error: "Пожалуйста, введите Ваш номер телефона"
			}
		}

		send.query(target, fieldsInfo, {}, 0); //создание запроса и отправка

		try {
			yaCounter.reachGoal(target.getAttribute('data-form-id')); //событие для yandex
		} catch (e) {}
	}
	
	window.setSendButton = function() {
		$(".sendButton").on("click", function(event) {
			window.sendMail(event.target);
		});
	}
	
	$(document).ready( function() {
		window.setSendButton();
	});
})();

//---------------//
//МОДАЛЬНОЕ ОКНО
(function() {
	window.modalOrderHandler = function() {
		$(".modalCaller").on("click", function(event) {
				var target = event.target;
				
				var dataFormId = target.getAttribute("data-form-id");
				if (!dataFormId) dataFormId = "modalForm";
	
				var text = '<form><table>' +
					'<tr>' +
					'<td style = "text-align:right"><label for = "clientNameModal">Ваше имя:</label></td><td><input type = "text" size = "20" data-id = "clientName" id = "clientNameModal" value = "Ваше имя" class = "wordsOnly" onclick = "if (this.value == ' + "'Ваше имя'" + ') this.value = ' + "''" + ';" maxlength = "50"></td>' +
					'</tr><tr>' +
					'<td><label for = "clientPhoneModal">Ваш номер:</label></td><td><input type = "text" size = "20" data-id = "clientPhone" id = "clientPhoneModal" value = "+" class = "clientPhone" maxlength = "20"></td>' +
					'</tr><tr>' +
					'<td colspan = "2"><div style = "padding:15px;margin:15px auto 0px auto;display:block;" class = "sendButton button" data-form-id = "' + dataFormId + '">Отправить</div></td>' +
				'</tr></table></form>';

				window.modalForm.showModal(text);
				window.setSendButton();
				window.setMask();
				
				onlyWords(document.getElementsByClassName("wordsOnly"));
			}
		);
	}
	
	window.modalOrderHandler();
})();

onlyWords(document.getElementsByClassName("wordsOnly"));
window.setSendButton();