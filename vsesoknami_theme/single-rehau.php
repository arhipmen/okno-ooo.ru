<!DOCTYPE HTML>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <title>
        <?php if (is_home() || is_front_page()) { bloginfo("name"); if (get_bloginfo("description") != "") { echo " - "; bloginfo("description"); } } else { bloginfo("name"); wp_title(); } ?>
    </title>
    <link rel="SHORTCUT ICON" href="<?php bloginfo('stylesheet_directory'); ?>/favicon.ico" type="image/x-icon">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/_invar/res/css/normalize.css" type="text/css">
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/_invar/res/css/style.css" type="text/css">
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/_res/css/style.css" type="text/css">
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/_components/slider/nivo-slider.css" type="text/css">
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/_components/slider/themes/default/default.css" media="screen" type="text/css">
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/_res/css/elements.css" type="text/css">
    <script src="<?php bloginfo('stylesheet_directory'); ?>/_invar/sys/plugins/jquery-latest.js"></script>
    <script src="<?php bloginfo('stylesheet_directory'); ?>/_invar/sys/plugins/jquery.min.js"></script>
    <script src="<?php bloginfo('stylesheet_directory'); ?>/_invar/sys/plugins/jquery.malihu.PageScroll2id.js"></script>
    <script src="<?php bloginfo('stylesheet_directory'); ?>/_invar/sys/plugins/jquery.maskedinput.min.js"></script>
    <script src="http://vsesoknami.ru/_invar/sys/plugins/easytab/ajquery.hashchange.min.js"></script>
    <script src="<?php bloginfo('stylesheet_directory'); ?>/_invar/sys/plugins/easytab/jquery.easytabs.min.js"></script>
    <script src="<?php bloginfo('stylesheet_directory'); ?>/_invar/sys/js/lib.js"></script>
    <script src="<?php bloginfo('stylesheet_directory'); ?>/_components/slider/jquery.nivo.slider.pack.js"></script>
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/style.css" type="text/css">
<?php wp_head(); ?>
	<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i,cid){w[l]=w[l]||[];w.pclick_client_id=cid;w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:''; j.async=true; j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-P23G9N', '80705');</script>
<!-- End Google Tag Manager -->
</head>

<body theme-url="<?php bloginfo('stylesheet_directory'); ?>/">

    <span id="upButton"></span>

    <div id="header">

        <!--Верхний блок-->
        <div class="contentCarrier">
            <div class="left">
                <a href="<?php bloginfo("url"); ?>/"><img src = "<?php bloginfo('stylesheet_directory'); ?>/_res/img/logo.png" alt = "<?php bloginfo("name"); ?>"></a>
            </div>
            <div class="right">
                <img src="<?php bloginfo('stylesheet_directory'); ?>/_res/img/phone.png" alt="Заказать" id="topPhone" class="modalCaller">
                <span id="phoneNumber"><a href="tel:<?php echo clerphone(get_field("topphone","params")); ?>"><?php echo get_field("topphone","params"); ?></a></span>
                <span id="workTime"><?php echo get_field("toptimework","params"); ?></span>
                <span id="topOrder" class="modalCaller" data-form-id="topModalForm">Заказать обратный звонок</span> </div>
            <div class="content">
                <div id="topForm">
                    <div class="modalCaller button">Перезвоните мне</div>
                    <div class="modalCaller button">Бесплатный замер</div>
                </div>
            </div>
        </div>
        <!--/Верхний блок-->

        <!--Главное меню-->
    
        <div id="mainMenuCarrier">
            <div id="mainMenu">
                <ul>
                    <li data-block-id="topAdvantages"><a href="<?php echo get_permalink(128); ?>" class="scroller">Акции</a></li>
                    <li data-block-id="mainBlock"><a href="<?php bloginfo("url"); ?>/#mainBlock" class="scroller">Окна Rehau</a></li>
                    <li data-block-id="aboutBlock"><a href="<?php bloginfo("url"); ?>/#calculatorBlock" class="scroller">Калькулятор</a></li>
                    <li data-block-id="priceBlock"><a href="<?php bloginfo("url"); ?>/#priceBlock" class="scroller">Цены</a></li>
                    <li data-block-id="advantagesBlock"><a href="<?php bloginfo("url"); ?>/#advantagesBlock" class="scroller">О нас</a></li>
                    <li data-block-id="galleryBlock"><a href="<?php bloginfo("url"); ?>/#galleryBlock" class="scroller">Фотогалерея</a></li>
                    <li data-block-id="contactsBlock"><a href="<?php bloginfo("url"); ?>/#contactsBlock" class="scroller">Контакты</a></li>
                </ul>
            </div>
        </div>

        <div id="topAdvantages">
            <ul>
                <?php $benefits = get_field("benefits",2); foreach($benefits as $benefit) { ?><li><?php echo $benefit['text']; ?></li><?php } ?>
            </ul>
        </div>
        <!--/Главное меню-->
    </div>
    <!--Основная часть-->
    <div id="carrier">
        <div class="contentCarrier">
            <!--Слайдер-->
            <div id="sliderCarrier" class="slider-wrapper theme-default" style="height:222px">
                <div id="slider" class="nivoSlider" style="height:222px">
					<?php $num = 0; $slides = get_field("slides",2); foreach($slides as $slide) { $num++; ?><a href=''><img src = '<?php echo $slide['image']['sizes']['headerslide']; ?>' alt = '<?php echo $slide['title']; ?>' title = '#htmlcaption_<?php echo $num; ?>'></a><?php } ?>
				</div>
            </div>
            <!--Конец слайдера-->
            <div id="mainBlock" class="mainBlock">
                <h1><?php the_title(); ?></h1>
				<div class="pagewindows">
<?php if (have_posts()) : while (have_posts()) : the_post(); the_content(); endwhile; endif; wp_reset_query(); ?>
				</div>
                <div class="clear"></div>
				<div id="contactsBlock">
					<div class="block">
						<div id="contacts">
							<div id="contactsForm">
								<span>Узнайте первым о всех скидках на окна <?php the_title(); ?></span>
								<form action="" method="post">
									Ваше имя:
									<input data-id="clientName" type="text" value="Ваше имя" class="clientName wordsOnly" style="margin-right:23px"> Ваш номер:
									<input data-id="clientPhone" type="text" value="+7" class="clientPhone">
									<div class="sendButton button">Отправить</div>
								</form>
							</div>
						</div>
					</div>
				</div>
            </div>
        </div>
    </div>
    <!--/Основная часть-->
    <div id="footer">
    
        <!--Нижний блок-->
        <div class="contentCarrier">
            <div class="carrier">
                <div class="left">
                    <div id="pay">
                        <div style="margin-bottom:7px">Мы принимаем к оплате:</div>
                        <img src="<?php bloginfo('stylesheet_directory'); ?>/_res/img/pay_cards.png" alt="">
                    </div>
                    <div style="margin-bottom:7px">Поделиться:</div>
                    <script type="text/javascript" src="http://yastatic.net/share/share.js" charset="utf-8"></script>
                    <div class="yashare-auto-init" data-yashareL10n="ru" data-yashareQuickServices="vkontakte,facebook,twitter" data-yashareTheme="counter"></div>
                </div>
                <div class="right">
                    <span class="textPhone"><?php echo get_field("botphone","params"); ?></span> <?php echo get_field("botcopyrights","params"); ?>
                    <!--<span style = "display:block;margin-top:30px;float:right" href = "http://tulboks.ru/" target = "blank"><img src = "/_res/img/tb.png"></span>-->
                    <!-- RedConnect -->
                    <script id="rhlpscrtg" type="text/javascript" charset="utf-8" async="async" src="https://web.redhelper.ru/service/main.js?c=rehaurus"></script>
                    <div style="display: none"><a class="rc-copyright" href="http://redconnect.ru">Обратный звонок RedConnect</a></div>
                    <!--/RedConnect -->
                </div>
                <div class="content">
                    <?php echo get_field("bottitle","params"); ?>
                    <div class="modalCaller button"><?php echo get_field("botbutton","params"); ?></div>
                </div>
            </div>
        </div>
        <!--/Нижний блок-->
    </div>
    <script src="<?php bloginfo('stylesheet_directory'); ?>/scripts.js"></script>
    <script src="<?php bloginfo('stylesheet_directory'); ?>/_invar/sys/js/script.js"></script>
    <script src="<?php bloginfo('stylesheet_directory'); ?>/_res/js/script.js"></script>
    <script type="text/jscript" src="https://neocomms.ru/callback/callback.js"></script>
    <!--
	<script type="text/javascript">(window.Image ? (new Image()) : document.createElement('img')).src = location.protocol + '//vk.com/rtrg?r=al60W2WbnvnVM7iSK0yUqxEV*/EjNaREDrK3tbm/FfcUBIjhdGBKx0Q9Vv8ABiXaXBQQsbNy0SdY95*FloKoaH255WYaIwHeWJu*BP6VQnzb*bdL9I2RrQ57wT4bfhVR*QIrOabzgaU1Wj2Dk8ymt6ECGKvYpaSobfgqZIQs6Ec-';</script>-->
<?php echo get_field("code","params"); ?>
<?php wp_footer(); ?>
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-P23G9N"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
</body>

</html>