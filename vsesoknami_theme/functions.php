<?php add_action('admin_head', 'custom_styles');
function custom_styles() { echo '<style>#toplevel_page_cptui_main_menu,#toplevel_page_acf-options,#categorydiv,#tagsdiv-post_tag,#menu-comments,#toplevel_page_edit-post_type-acf-field-group,#menu-plugins,#toplevel_page_meowapps-main-menu,body h2 a.dashicons{display:none!important</style><script type="text/javascript">jQuery(document).ready(function(){var text=jQuery("#menu-posts .wp-menu-name").html();text=text.replace("Записи","Акции");jQuery("#menu-posts .wp-menu-name").html(text);jQuery("#menu-posts ul li a.wp-first-item").text("Все");jQuery("#menu-posts li").eq(4).remove();jQuery("#menu-posts li").eq(3).remove();});</script>'; }

function wph_admin_footer_text(){echo'<i>Программирование и адаптация на WordPress - <a href="//www.pdnx.ru">Кирилл Чулыгин</a></i>';}add_filter('admin_footer_text', 'wph_admin_footer_text');

if( function_exists('acf_add_options_page') ) {
	acf_add_options_page(array('page_title' => __("Дополнительные настройки"),'menu_title' => __("Доп. настройки"),'menu_slug' => 'params', 'post_id' => 'params'));
}

function clerphone($phone) {
	$phone = str_replace(" ","",$phone);
	$phone = str_replace("-","",$phone);
	$phone = str_replace("(","",$phone);
	$phone = str_replace(")","",$phone);
	return strip_tags($phone);
}

add_image_size('headerslide', 1920, 801, true);
add_image_size('headerslide_prev', 300, 125, true);
add_image_size('discount', 350, 515, false);
add_image_size('discount_prev', 82, 120, false);
add_image_size('gallery', 400, 150, false);
add_image_size('stock', 300, 200, true);
add_image_size('windows', 160, 250, false);

function html2rgb($color){
    if ($color[0] == '#')
        $color = substr($color, 1);

    if (strlen($color) == 6)
        list($r, $g, $b) = array($color[0].$color[1],
                                 $color[2].$color[3],
                                 $color[4].$color[5]);
    elseif (strlen($color) == 3)
        list($r, $g, $b) = array($color[0].$color[0], $color[1].$color[1], $color[2].$color[2]);
    else
        return false;

    $r = hexdec($r); $g = hexdec($g); $b = hexdec($b);

    return array($r, $g, $b);
}

function sanitize_text($text){
	$gost = array(
	   "Є"=>"EH","І"=>"I","і"=>"i","№"=>"#","є"=>"eh",
	   "А"=>"A","Б"=>"B","В"=>"V","Г"=>"G","Д"=>"D",
	   "Е"=>"E","Ё"=>"JO","Ж"=>"ZH",
	   "З"=>"Z","И"=>"I","Й"=>"JJ","К"=>"K","Л"=>"L",
	   "М"=>"M","Н"=>"N","О"=>"O","П"=>"P","Р"=>"R",
	   "С"=>"S","Т"=>"T","У"=>"U","Ф"=>"F","Х"=>"KH",
	   "Ц"=>"C","Ч"=>"CH","Ш"=>"SH","Щ"=>"SHH","Ъ"=>"'",
	   "Ы"=>"Y","Ь"=>"","Э"=>"EH","Ю"=>"YU","Я"=>"YA",
	   "а"=>"a","б"=>"b","в"=>"v","г"=>"g","д"=>"d",
	   "е"=>"e","ё"=>"jo","ж"=>"zh",
	   "з"=>"z","и"=>"i","й"=>"jj","к"=>"k","л"=>"l",
	   "м"=>"m","н"=>"n","о"=>"o","п"=>"p","р"=>"r",
	   "с"=>"s","т"=>"t","у"=>"u","ф"=>"f","х"=>"kh",
	   "ц"=>"c","ч"=>"ch","ш"=>"sh","щ"=>"shh","ъ"=>"",
	   "ы"=>"y","ь"=>"","э"=>"eh","ю"=>"yu","я"=>"ya",
	   "—"=>"-","«"=>"","»"=>"","…"=>""
	);
	return strtr($text, $gost);
}

remove_action( 'add_option_new_admin_email', 'update_option_new_admin_email' );
remove_action( 'update_option_new_admin_email', 'update_option_new_admin_email' );

function wpdocs_update_option_new_admin_email( $old_value, $value ) {

    update_option( 'admin_email', $value );
}
add_action( 'add_option_new_admin_email', 'wpdocs_update_option_new_admin_email', 10, 2 );
add_action( 'update_option_new_admin_email', 'wpdocs_update_option_new_admin_email', 10, 2 );

function gp_remove_cpt_slug( $post_link, $post ) {
    if ( 'rehau' === $post->post_type && 'publish' === $post->post_status ) {
        $post_link = str_replace( '/' . $post->post_type . '/', '/', $post_link );
    }
    return $post_link;
}
add_filter( 'post_type_link', 'gp_remove_cpt_slug', 10, 2 );

function gp_add_cpt_post_names_to_main_query( $query ) {
	if ( ! $query->is_main_query() ) {
		return;
	}
	if ( ! isset( $query->query['page'] ) || 2 !== count( $query->query ) ) {
		return;
	}
	if ( empty( $query->query['name'] ) ) {
		return;
	}
	$query->set( 'post_type', array( 'post', 'page', 'rehau' ) );
}
add_action( 'pre_get_posts', 'gp_add_cpt_post_names_to_main_query' );

add_editor_style( 'editor_styles.css' );

function fresh_editor_style($css){
	global $editor_styles;
	if (empty($css) or empty($editor_styles)){
		return $css;
	}
	$mce_css = array();
	$style_uri = get_stylesheet_directory_uri();
	$style_dir = get_stylesheet_directory();
	if (is_child_theme()){
		$template_uri = get_template_directory_uri();
		$template_dir = get_template_directory();
		foreach ($editor_styles as $key => $file){
			if ($file && file_exists( "$template_dir/$file" )){
				$mce_css[] = add_query_arg(
					'version',
					filemtime( "$template_dir/$file" ),
					"$template_uri/$file"
				);
			}
		}
	}
	foreach ($editor_styles as $file){
		if ($file && file_exists( "$style_dir/$file" )){
			$mce_css[] = add_query_arg(
				'version',
				filemtime( "$style_dir/$file" ),
				"$style_uri/$file"
			);
		}
	}
	return implode( ',', $mce_css );
}

add_filter('mce_css', 'fresh_editor_style');

function calc_func(){
	 return '<h2>Калькулятор стоимости окон '.get_the_title(get_the_ID()).'</h2><div id="calculatorBlock">
                <div class="block">
                    <div class="left">
                        <div id="modelMenu" data-profile="blitz">
                            <span class="title">&nbsp;</span>
                            <ul id="modelList" style="">
                                <li data-profile="blitz" class="profile">Blitz</li>
                                <li data-profile="sib" class="profile">Sib</li>
                                <li data-profile="delight" class="profile">Delight</li>
                                <li data-profile="brilliant" class="profile">Brilliant</li>
                            </ul>
                        </div>
                        <div id="profile">
                            <img id="currentImage" width="250" src="'.get_bloginfo('stylesheet_directory').'/_components/calculator/profiles/blitz.jpg" alt="">
                        </div>
                    </div>
                    <div class="right">
                        <span class="title">Узнать стоимость<br/> <br/>Выберите параметры Вашего окна и нажмите "Узнать <br/>точную цену"<br/> <br/><small>Мы рассчитаем для Вас точную цену<br/>с учетом действующей скидки:</small></span>
                        <div id="totalPrice" class="clear">0 р.</div>
                        <form action="#" method="post">
                            <div class="recall">
                                Ваше имя:<br/>
                                <input value="Ваше имя" data-id="clientName" maxlength="30" class="wordsOnly clientName" type="text" /><br/>Ваш номер:<br/>
                                <input value="+7" data-id="clientPhone" class="clientPhone" type="text" />
                                <div class="sendButton button">Узнать точную цену</div>
                            </div>
                        </form>
                    </div>
                    <div class="content">
                        <span class="title">Параметры окна</span>
                        <div id="profileList">
                            <ul>
                                <li class="leaf"><img src="'.get_bloginfo('stylesheet_directory').'/_components/calculator/leafs/leaf1/1_stvorka_1.png" class="leafsMenu" data-leaf="leaf1" alt="" data-leaf-num="0" / style="border-color:#9f005e">
                                    <div class="drop">
                                        <ul>
                                            <li><img src="'.get_bloginfo('stylesheet_directory').'/_components/calculator/leafs/leaf1/1_stvorka_1.png" class="leafsMenu" data-leaf="leaf1" alt="" data-leaf-num="0" /> </li>
                                            <li><img src="'.get_bloginfo('stylesheet_directory').'/_components/calculator/leafs/leaf1/1_stvorka_2.png" class="leafsMenu" data-leaf="leaf1" alt="" data-leaf-num="1" /> </li>
                                            <li><img src="'.get_bloginfo('stylesheet_directory').'/_components/calculator/leafs/leaf1/1_stvorka_3.png" class="leafsMenu" data-leaf="leaf1" alt="" data-leaf-num="2" /></li>
                                        </ul>
                                    </div>
                                </li>
                                <li class="leaf"><img src="'.get_bloginfo('stylesheet_directory').'/_components/calculator/leafs/leaf2/2_stvorka_1.png" class="leafsMenu" data-leaf="leaf2" alt="" data-leaf-num="0" />
                                    <div class="drop">
                                        <ul>
                                            <li><img src="'.get_bloginfo('stylesheet_directory').'/_components/calculator/leafs/leaf2/2_stvorka_1.png" class="leafsMenu" data-leaf="leaf2" alt="" data-leaf-num="0" /> </li>
                                            <li><img src="'.get_bloginfo('stylesheet_directory').'/_components/calculator/leafs/leaf2/2_stvorka_2.png" class="leafsMenu" data-leaf="leaf2" alt="" data-leaf-num="1" /> </li>
                                            <li><img src="'.get_bloginfo('stylesheet_directory').'/_components/calculator/leafs/leaf2/2_stvorka_3.png" class="leafsMenu" data-leaf="leaf2" alt="" data-leaf-num="2" /> </li>
                                            <li><img src="'.get_bloginfo('stylesheet_directory').'/_components/calculator/leafs/leaf2/2_stvorka_4.png" class="leafsMenu" data-leaf="leaf2" alt="" data-leaf-num="2" /> </li>
                                        </ul>
                                    </div>
                                </li>
                                <li class="leaf"><img src="'.get_bloginfo('stylesheet_directory').'/_components/calculator/leafs/leaf3/3_stvorka_1.png" class="leafsMenu" data-leaf="leaf3" alt="" data-leaf-num="0" />
                                    <div class="drop">
                                        <ul>
                                            <li><img src="'.get_bloginfo('stylesheet_directory').'/_components/calculator/leafs/leaf3/3_stvorka_1.png" class="leafsMenu" data-leaf="leaf3" alt="" data-leaf-num="0" /> </li>
                                            <li><img src="'.get_bloginfo('stylesheet_directory').'/_components/calculator/leafs/leaf3/3_stvorka_2.png" class="leafsMenu" data-leaf="leaf3" alt="" data-leaf-num="1" /> </li>
                                            <li><img src="'.get_bloginfo('stylesheet_directory').'/_components/calculator/leafs/leaf3/3_stvorka_3.png" class="leafsMenu" data-leaf="leaf3" alt="" data-leaf-num="2" /> </li>
                                            <li><img src="'.get_bloginfo('stylesheet_directory').'/_components/calculator/leafs/leaf3/3_stvorka_4.png" class="leafsMenu" data-leaf="leaf3" alt="" data-leaf-num="2" /> </li>
                                        </ul>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div id="verticalScrollCarrier">
                            <div id="verticalScroll">
                                <div id="verticalBox" class="box" data-type="0"></div>
                            </div>
                        </div>
                        <div id="currentLeafCarrier"><img id="currentLeaf" src="'.get_bloginfo('stylesheet_directory').'/_components/calculator/leafs/big/leaf1/1_stvorka_1.png" alt=""></div>
                        <div id="horizontalScrollCarrier">
                            <div id="horizontalScroll">
                                <div id="horizontalBox" class="box" data-type="1"></div>
                            </div>
                        </div>
                        <form>
                            <input type="text" id="windowHeight" class="numsOnly" size="4" maxlength="4" value="500" /><img height="24" src="'.get_bloginfo('stylesheet_directory').'/_res/img/x.png" style="padding-top:10px;margin:0px 10px 0px 10px;" alt="">
                            <input class="numsOnly" type="text" id="windowWidth" style="margin-right:5px" size="4" maxlength="4" value="500" />мм</form>
                    </div>
                    <div class="clear"></div>
                </div>
                <script src="'.get_bloginfo('stylesheet_directory').'/_components/calculator/calculator.js"></script>
            </div>';
}
add_shortcode('calc', 'calc_func');