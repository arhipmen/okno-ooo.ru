<!DOCTYPE HTML>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <title>
        <?php if (is_home() || is_front_page()) { bloginfo("name"); if (get_bloginfo("description") != "") { echo " &raquo; "; bloginfo("description"); } } else { bloginfo("name"); wp_title(); } ?>
    </title>
    <link rel="SHORTCUT ICON" href="<?php bloginfo('stylesheet_directory'); ?>/favicon.ico" type="image/x-icon">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/_invar/res/css/normalize.css" type="text/css">
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/_invar/res/css/style.css" type="text/css">
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/_res/css/style.css" type="text/css">
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/_components/slider/nivo-slider.css" type="text/css">
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/_components/slider/themes/default/default.css" media="screen" type="text/css">
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/_res/css/elements.css" type="text/css">
    <script src="<?php bloginfo('stylesheet_directory'); ?>/_invar/sys/plugins/jquery-latest.js"></script>
    <script src="<?php bloginfo('stylesheet_directory'); ?>/_invar/sys/plugins/jquery.min.js"></script>
    <script src="<?php bloginfo('stylesheet_directory'); ?>/_invar/sys/plugins/jquery.malihu.PageScroll2id.js"></script>
    <script src="<?php bloginfo('stylesheet_directory'); ?>/_invar/sys/plugins/jquery.maskedinput.min.js"></script>
    <script src="http://vsesoknami.ru/_invar/sys/plugins/easytab/ajquery.hashchange.min.js"></script>
    <script src="<?php bloginfo('stylesheet_directory'); ?>/_invar/sys/plugins/easytab/jquery.easytabs.min.js"></script>
    <script src="<?php bloginfo('stylesheet_directory'); ?>/_invar/sys/js/lib.js"></script>
    <script src="<?php bloginfo('stylesheet_directory'); ?>/_components/slider/jquery.nivo.slider.pack.js"></script>
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/style.css" type="text/css">
<?php wp_head(); ?>
	<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i,cid){w[l]=w[l]||[];w.pclick_client_id=cid;w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:''; j.async=true; j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-P23G9N', '80705');</script>
<!-- End Google Tag Manager -->
</head>

<body theme-url="<?php bloginfo('stylesheet_directory'); ?>/">

    <span id="upButton"></span>

    <div id="header">

        <!--Верхний блок-->
        <div class="contentCarrier">
            <div class="left">
                <a href="<?php bloginfo("url"); ?>/"><img src = "<?php bloginfo('stylesheet_directory'); ?>/_res/img/logo.png" alt = "<?php bloginfo("name"); ?>"></a>
            </div>
            <div class="right">
                <img src="<?php bloginfo('stylesheet_directory'); ?>/_res/img/phone.png" alt="Заказать" id="topPhone" class="modalCaller">
                <span id="phoneNumber"><a href="tel:<?php echo clerphone(get_field("topphone","params")); ?>"><?php echo get_field("topphone","params"); ?></a></span>
                <span id="workTime"><?php echo get_field("toptimework","params"); ?></span>
                <span id="topOrder" class="modalCaller" data-form-id="topModalForm">Заказать обратный звонок</span> </div>
            <div class="content">
                <div id="topForm">
                    <div class="modalCaller button">Перезвоните мне</div>
                    <div class="modalCaller button">Бесплатный замер</div>
                </div>
            </div>
        </div>
        <!--/Верхний блок-->

        <!--Главное меню-->
    
        <div id="mainMenuCarrier">
            <div id="mainMenu">
                <ul>
                    <li data-block-id="topAdvantages"><a href="<?php echo get_permalink(128); ?>" class="scroller">Акции</a></li>
                    <li data-block-id="mainBlock"><a href="#mainBlock" class="scroller">Окна Rehau</a></li>
                    <li data-block-id="aboutBlock"><a href="#calculatorBlock" class="scroller">Калькулятор</a></li>
                    <li data-block-id="priceBlock"><a href="#priceBlock" class="scroller">Цены</a></li>
                    <li data-block-id="advantagesBlock"><a href="#advantagesBlock" class="scroller">О нас</a></li>
                    <li data-block-id="galleryBlock"><a href="#galleryBlock" class="scroller">Фотогалерея</a></li>
                    <li data-block-id="contactsBlock"><a href="#contactsBlock" class="scroller">Контакты</a></li>
                </ul>
            </div>
        </div>

        <div id="topAdvantages">
            <ul>
                <?php $benefits = get_field("benefits",get_the_ID()); foreach($benefits as $benefit) { ?><li><?php echo $benefit['text']; ?></li><?php } ?>
            </ul>
        </div>
        <!--/Главное меню-->
    </div>
    <!--Основная часть-->
    <div id="carrier">
        <div class="contentCarrier">
            <!--Слайдер-->
            <div id="sliderCarrier" class="slider-wrapper theme-default">
                <div id="slider" class="nivoSlider">
					<?php $num = 0; $slides = get_field("slides",get_the_ID()); foreach($slides as $slide) { $num++; ?><a href=''><img src = '<?php echo $slide['image']['sizes']['headerslide']; ?>' alt = '<?php echo $slide['title']; ?>' title = '#htmlcaption_<?php echo $num; ?>'></a><?php } ?>
				</div>
            </div>
            <?php $num = 0; $slides = get_field("slides",get_the_ID()); foreach($slides as $slide) { $num++; ?><div id='htmlcaption_<?php echo $num; ?>' class='nivo-html-caption'>
                <div class="nivoBlockCarrier">
                    <a class="nivo-prevNav" id="prevNav"></a>
                    <a class="nivo-nextNav" id="nextNav"></a>
                    <div class="block">
                        <div class="right"<?php if ($slide['color'] != "") { $color = html2rgb($slide['color']); ?> style="background-color:rgba(<?php echo $color[0]; ?>,<?php echo $color[1]; ?>,<?php echo $color[2]; ?>,0.8)"<?php } ?>>
                            <?php if ($slide['title'] != "") { ?><span class="title"><?php echo $slide['title']; ?></span><?php } ?>
                            <?php if ($slide['subtitle'] != "") { ?><span class="cond"><?php echo $slide['subtitle']; ?></span><?php } ?>
                            <?php if ($slide['desc'] != "") { ?><hr><span class="cond2"><?php echo $slide['desc']; ?></span><hr><?php } ?>
                            <?php if ($slide['form'] == "1") { ?><div class="sliderForm">
                                <form action="" method="post">
                                    Ваше имя:
                                    <input data-id="clientName" type="text" value="Ваше имя" class="clientName wordsOnly" style="margin-right:23px"> Ваш номер:
                                    <input data-id="clientPhone" type="text" value="+7" class="clientPhone">
                                    <div class="sendButton button">Узнать подробности акции</div>
                                </form>
                            </div><?php } ?>
                        </div>
                    </div>
                </div>
            </div><?php } ?>
            <!--Конец слайдера-->
            <div id="mainBlock">
                <h1><?php echo get_field("infoblock_title",get_the_ID()); ?></h1>
				<div class="windows">
<?php $num = 0; $args = array(
	'posts_per_page' => -1, 
	'post_type' => 'rehau'
); $newquery = new WP_Query($args);
					if ($newquery->have_posts()): while ($newquery->have_posts()) : $newquery->the_post(); $num++; ?><a href="<?php the_permalink(); ?>" class="item<?php if ($num == 1) { echo ' first'; } ?>">
						<span class="head"><?php the_title(); ?></span>
						<span class="desc"><?php echo get_field("desc",get_the_ID()); ?></span>
						<span class="prev"><img src="<?php $prev = get_field("prev",get_the_ID()); echo $prev['sizes']['windows']; ?>" alt="" border="0" /></span>
						<span class="price">
							<?php if (get_field("oldprice",get_the_ID()) != "") { ?><strike>&nbsp;от <?php echo number_format(get_field("oldprice",get_the_ID()), 0, '', ' '); ?> р.&nbsp;</strike><?php } ?>
							от <?php echo number_format(get_field("price",get_the_ID()), 0, '', ' '); ?> р.
						</span>
					</a><?php if ($num == 4) { $num = 0; echo '<div class="clear"></div>'; } endwhile; endif; wp_reset_query(); ?>
				</div>
                <!--<div class="tab-container" id="tabContainer">
                    <div id="mainBlockMenu">
                        <ul class="etabs">
                            <?php $infoblock = get_field("infoblock",get_the_ID()); foreach($infoblock as $info) { ?><li class="tab"><a href="#tab<?php echo sanitize_text($info['name']); ?>"><?php echo $info['name']; ?></a></li><?php } ?>
                        </ul>
                    </div>
                    <?php $infoblock = get_field("infoblock",get_the_ID()); foreach($infoblock as $info) { ?><div id="tab<?php echo sanitize_text($info['name']); ?>" class="tabEl block">
                        <div class='left'>
                            <h2><?php echo $info['title']; ?></h2>
                            <img src="<?php echo $info['prev']['sizes']['discount']; ?>" alt="">
                        </div>
                        <div class='content'>
                            <ul>
                                <li><?php echo str_replace("\n","</li><li>",$info['benefits']); ?></li>
                            </ul>
                            <div class="mainBlockForm">
                                <div class="left">
                                    <span>Старая цена: <span class = "oldPrice"><?php echo number_format($info['oldprice'], 0, '', ' '); ?> р.</span></span>
                                    <span class="priceTitle">Новая цена</span>
                                    <span class="newPrice"><?php echo number_format($info['newprice'], 0, '', ' '); ?> р.</span>
                                </div>
                                <div class='content'>
                                    <div class="modalCaller button">
                                        Заказать окно
                                        <br>со скидкой
                                    </div>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                    </div><?php } ?>
                </div>-->
                <div class="clear"></div>
            </div>
            <div id="calculatorBlock">
                <h2><?php echo get_field("calc_title",get_the_ID()); ?></h2>
                <div class="block">
                    <div class="left">
                        <div id="modelMenu" data-profile="blitz">
                            <span class="title">Модель: <span id = "profileTitle">Blitz</span></span>
                            <ul id="modelList" style="">
                                <li data-profile="blitz" class="profile">Blitz</li>
                                <li data-profile="grazio" class="profile">Grazio</li>
                                <li data-profile="delight" class="profile">Delight</li>
                                <li data-profile="brilliant" class="profile">Brilliant</li>
                                <li data-profile="intelio" class="profile">Intelio</li>
                                <li data-profile="tandem" class="profile">Tandem</li>
                                <li data-profile="geneo" class="profile">Geneo</li>
                            </ul>
                        </div>
                        <div id="profile">
                            <img id="currentImage" width="250" src="<?php bloginfo('stylesheet_directory'); ?>/_components/calculator/profiles/1.jpg" alt="">
                        </div>
                    </div>
                    <div class="right">
                        <span class="title">Узнать стоимость<br/> <br/>Выберите параметры Вашего окна и нажмите "Узнать <br/>точную цену"<br/> <br/><small>Мы рассчитаем для Вас точную цену<br/>с учетом действующей скидки:</small></span>
                        <div id="totalPrice" class="clear">0 р.</div>
                        <form action="#" method="post">
                            <div class="recall">
                                Ваше имя:<br/>
                                <input value="Ваше имя" data-id="clientName" maxlength="30" class="wordsOnly clientName" type="text" /><br/>Ваш номер:<br/>
                                <input value="+7" data-id="clientPhone" class="clientPhone" type="text" />
                                <div class="sendButton button">Узнать точную цену</div>
                            </div>
                        </form>
                    </div>
                    <div class="content">
                        <span class="title">Параметры окна</span>
                        <div id="profileList">
                            <ul>
                                <li class="leaf"><img src="<?php bloginfo('stylesheet_directory'); ?>/_components/calculator/leafs/leaf1/1_stvorka_1.png" class="leafsMenu" data-leaf="leaf1" alt="" data-leaf-num="0" / style="border-color:#9f005e">
                                    <div class="drop">
                                        <ul>
                                            <li><img src="<?php bloginfo('stylesheet_directory'); ?>/_components/calculator/leafs/leaf1/1_stvorka_1.png" class="leafsMenu" data-leaf="leaf1" alt="" data-leaf-num="0" /> </li>
                                            <li><img src="<?php bloginfo('stylesheet_directory'); ?>/_components/calculator/leafs/leaf1/1_stvorka_2.png" class="leafsMenu" data-leaf="leaf1" alt="" data-leaf-num="1" /> </li>
                                            <li><img src="<?php bloginfo('stylesheet_directory'); ?>/_components/calculator/leafs/leaf1/1_stvorka_3.png" class="leafsMenu" data-leaf="leaf1" alt="" data-leaf-num="2" /></li>
                                        </ul>
                                    </div>
                                </li>
                                <li class="leaf"><img src="<?php bloginfo('stylesheet_directory'); ?>/_components/calculator/leafs/leaf2/2_stvorka_1.png" class="leafsMenu" data-leaf="leaf2" alt="" data-leaf-num="0" />
                                    <div class="drop">
                                        <ul>
                                            <li><img src="<?php bloginfo('stylesheet_directory'); ?>/_components/calculator/leafs/leaf2/2_stvorka_1.png" class="leafsMenu" data-leaf="leaf2" alt="" data-leaf-num="0" /> </li>
                                            <li><img src="<?php bloginfo('stylesheet_directory'); ?>/_components/calculator/leafs/leaf2/2_stvorka_2.png" class="leafsMenu" data-leaf="leaf2" alt="" data-leaf-num="1" /> </li>
                                            <li><img src="<?php bloginfo('stylesheet_directory'); ?>/_components/calculator/leafs/leaf2/2_stvorka_3.png" class="leafsMenu" data-leaf="leaf2" alt="" data-leaf-num="2" /> </li>
                                            <li><img src="<?php bloginfo('stylesheet_directory'); ?>/_components/calculator/leafs/leaf2/2_stvorka_4.png" class="leafsMenu" data-leaf="leaf2" alt="" data-leaf-num="2" /> </li>
                                        </ul>
                                    </div>
                                </li>
                                <li class="leaf"><img src="<?php bloginfo('stylesheet_directory'); ?>/_components/calculator/leafs/leaf3/3_stvorka_1.png" class="leafsMenu" data-leaf="leaf3" alt="" data-leaf-num="0" />
                                    <div class="drop">
                                        <ul>
                                            <li><img src="<?php bloginfo('stylesheet_directory'); ?>/_components/calculator/leafs/leaf3/3_stvorka_1.png" class="leafsMenu" data-leaf="leaf3" alt="" data-leaf-num="0" /> </li>
                                            <li><img src="<?php bloginfo('stylesheet_directory'); ?>/_components/calculator/leafs/leaf3/3_stvorka_2.png" class="leafsMenu" data-leaf="leaf3" alt="" data-leaf-num="1" /> </li>
                                            <li><img src="<?php bloginfo('stylesheet_directory'); ?>/_components/calculator/leafs/leaf3/3_stvorka_3.png" class="leafsMenu" data-leaf="leaf3" alt="" data-leaf-num="2" /> </li>
                                            <li><img src="<?php bloginfo('stylesheet_directory'); ?>/_components/calculator/leafs/leaf3/3_stvorka_4.png" class="leafsMenu" data-leaf="leaf3" alt="" data-leaf-num="2" /> </li>
                                        </ul>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div id="verticalScrollCarrier">
                            <div id="verticalScroll">
                                <div id="verticalBox" class="box" data-type="0"></div>
                            </div>
                        </div>
                        <div id="currentLeafCarrier"><img id="currentLeaf" src="<?php bloginfo('stylesheet_directory'); ?>/_components/calculator/leafs/big/leaf1/1_stvorka_1.png" alt=""></div>
                        <div id="horizontalScrollCarrier">
                            <div id="horizontalScroll">
                                <div id="horizontalBox" class="box" data-type="1"></div>
                            </div>
                        </div>
                        <form>
                            <input type="text" id="windowHeight" class="numsOnly" size="4" maxlength="4" value="500" /><img height="24" src="<?php bloginfo('stylesheet_directory'); ?>/_res/img/x.png" style="padding-top:10px;margin:0px 10px 0px 10px;" alt="">
                            <input class="numsOnly" type="text" id="windowWidth" style="margin-right:5px" size="4" maxlength="4" value="500" />мм</form>
                    </div>
                    <div class="clear"></div>
                </div>

                <script src="<?php bloginfo('stylesheet_directory'); ?>/_components/calculator/calculator.js"></script>
            </div>
            <div id="priceBlock">
                <h2><?php echo get_field("price_title",get_the_ID()); ?></h2>
                <div class="block">
                    <span class="title"><?php echo get_field("price_desc",get_the_ID()); ?></span>
                    <div class="tab-container" id="priceTabContainer">
                        <div id="priceBlockMenu">
                            Тип профиля
                            <ul class="etabs">
                                <?php $price = get_field("price",get_the_ID()); foreach($price as $item) { ?><li class="tab"><a href="#price<?php echo sanitize_text($item['name']); ?>"><?php echo $item['name']; ?></a></li><?php } ?>
                            </ul>
                        </div>
                        <?php $price = get_field("price",get_the_ID()); foreach($price as $item) { ?><div id="price<?php echo sanitize_text($item['name']); ?>" class="tabEl2">
                            <img src="<?php echo $item['prev']['url']; ?>" alt="">
                            <table>
                                <tr>
                                    <td class="p1">Балконный блок</td>
                                    <td class="p2">Трехстворчатое окно</td>
                                    <td class="p3">Двухстворчатое окно</td>
                                </tr>
                                <tr>
                                    <td class="p1 price"><?php echo number_format($item['price1'], 0, '', ' '); ?> р.</td>
                                    <td class="p2 price"><?php echo number_format($item['price2'], 0, '', ' '); ?> р.</td>
                                    <td class="p3 price"><?php echo number_format($item['price3'], 0, '', ' '); ?> р.</td>
                                </tr>
                            </table>
                        </div><?php } ?>
                    </div>
                    <span class="cond"><?php echo get_field("price_text",get_the_ID()); ?></span>
                </div>
            </div>
            <div class="clear"></div>
            <div id="advantagesBlock">
                <h2><?php echo get_field("benefits_title",get_the_ID()); ?></h2>
                <div class="block">
                    <table>
                            <?php $num = 0; $benefits = get_field("benefits_block",get_the_ID()); foreach($benefits as $benefit) { $num++; if ($num == 1) { echo '<tr>'; } ?><td style="background-image:url('<?php echo $benefit['icon']['url']; ?>')"><?php echo $benefit['text']; ?></td><?php if ($num == 2) { $num = 0; echo '</tr>'; } } if ($num == 1) { echo '</tr>'; } ?>
                    </table>
                </div>
            </div>
            <div id="balkonyBlock" style="border-top:solid 1px #c6cbd1;">
                <h2><?php echo get_field("block_title",get_the_ID()); ?></h2>
                <div class="block">
                    <img src="<?php $image = get_field("block_image",get_the_ID()); echo $image['url']; ?>" alt="<?php echo get_field("block_title",get_the_ID()); ?>">
                </div>
            </div>
            <div id="galleryBlock">
                <?php $gallery = get_field("gallery",get_the_ID()); foreach($gallery as $image) { ?><a href="<?php echo $image['url']; ?>" class="gallery"><img src="<?php echo $image['url']; ?>" alt=""></a><?php } ?>
                <div class="clear"></div>
            </div>
            <div id="contactsBlock">
                <h2><?php echo get_field("contact_title",get_the_ID()); ?></h2>
                <?php echo get_field("contact_map",get_the_ID()); ?>
                <div class="block">
                    <div id="contacts">
                        <div id="contactsInfo">
                            <span class="addr"><?php echo get_field("contact_address",get_the_ID()); ?></span>
                            <span class="phone"><?php echo get_field("contact_phone",get_the_ID()); ?></span>
                        </div>
                        <div id="contactsForm">
                            <span><?php echo get_field("contact_form",get_the_ID()); ?></span>
                            <form action="" method="post">
                                Ваше имя:
                                <input data-id="clientName" type="text" value="Ваше имя" class="clientName wordsOnly" style="margin-right:23px"> Ваш номер:
                                <input data-id="clientPhone" type="text" value="+7" class="clientPhone">
                                <div class="sendButton button">Отправить</div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clear"></div>
            <div id="howBlock">
                <h2><?php echo get_field("block2_title",get_the_ID()); ?></h2>
                <div class="block">
                    <ul>
                        <?php $block2_info = get_field("block2_info",get_the_ID()); foreach($block2_info as $info) { ?><li style="background-image:url('<?php echo $info['icon']['url']; ?>')"><?php echo $info['text']; ?></li><?php } ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!--/Основная часть-->
    <div id="footer">
    
        <!--Нижний блок-->
        <div class="contentCarrier">
            <div class="carrier">
                <div class="left">
                    <div id="pay">
                        <div style="margin-bottom:7px">Мы принимаем к оплате:</div>
                        <img src="<?php bloginfo('stylesheet_directory'); ?>/_res/img/pay_cards.png" alt="">
                    </div>
                    <div style="margin-bottom:7px">Поделиться:</div>
                    <script type="text/javascript" src="http://yastatic.net/share/share.js" charset="utf-8"></script>
                    <div class="yashare-auto-init" data-yashareL10n="ru" data-yashareQuickServices="vkontakte,facebook,twitter" data-yashareTheme="counter"></div>
                </div>
                <div class="right">
                    <span class="textPhone"><?php echo get_field("botphone","params"); ?></span> <?php echo get_field("botcopyrights","params"); ?>
                    <!--<span style = "display:block;margin-top:30px;float:right" href = "http://tulboks.ru/" target = "blank"><img src = "/_res/img/tb.png"></span>-->
                    <!-- RedConnect -->
                    <script id="rhlpscrtg" type="text/javascript" charset="utf-8" async="async" src="https://web.redhelper.ru/service/main.js?c=rehaurus"></script>
                    <div style="display: none"><a class="rc-copyright" href="http://redconnect.ru">Обратный звонок RedConnect</a></div>
                    <!--/RedConnect -->
                </div>
                <div class="content">
                    <?php echo get_field("bottitle","params"); ?>
                    <div class="modalCaller button"><?php echo get_field("botbutton","params"); ?></div>
                </div>
            </div>
        </div>
        <!--/Нижний блок-->
    </div>
    <script src="<?php bloginfo('stylesheet_directory'); ?>/scripts.js"></script>
    <script src="<?php bloginfo('stylesheet_directory'); ?>/_invar/sys/js/script.js"></script>
    <script src="<?php bloginfo('stylesheet_directory'); ?>/_res/js/script.js"></script>
    <script type="text/jscript" src="https://neocomms.ru/callback/callback.js"></script>
    <!--
	<script type="text/javascript">(window.Image ? (new Image()) : document.createElement('img')).src = location.protocol + '//vk.com/rtrg?r=al60W2WbnvnVM7iSK0yUqxEV*/EjNaREDrK3tbm/FfcUBIjhdGBKx0Q9Vv8ABiXaXBQQsbNy0SdY95*FloKoaH255WYaIwHeWJu*BP6VQnzb*bdL9I2RrQ57wT4bfhVR*QIrOabzgaU1Wj2Dk8ymt6ECGKvYpaSobfgqZIQs6Ec-';</script>-->
<?php echo get_field("code","params"); ?>
<?php wp_footer(); ?>
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-P23G9N"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
</body>

</html>