(function() {
	window.createXmlHttpRequestObject = function() { //�������� ������� �������
		var xmlHttp;
		try {
			xmlHttp = new XMLHttpRequest();
		} catch(e) {
			var xmlHttpVersions = new Array('MSXML2.XMLHTTP.6.0', 'MSXML2.XMLHTTP.5.0', 'MSXML2.XMLHTTP.4.0', 'MSXML2.XMLHTTP.3.0', 'MSXML2.XMLHTTP', 'Microsoft.XMLHTTP');
			
			for (var i = 0; i < xmlHttpVersions.length && !xmlHttp; i++){
				try {
					xmlhttp = new ActiveXObject(xmlVersions[i]);
					
				} catch(e) {}
			}
		}
		
		if (xmlHttp){
			return xmlHttp;
		} else {
		//	throw new Error("no xmlHttp was created");
		}
	}

	window.createMsxml2DOMDocumentObject = function() {
		var msxml2DOM; //������ �� ������ msxml
		var msxml2DOMDocumentVersions = new Array(
			"msxml2.DOMDocument.6.0", 
			"msxml2.DOMDocument.5.0", 
			"msxml2.DOMDocument.4.0", 
			"msxml2.DOMDocument.3.0", 
			"msxml2.DOMDocument", 
			"msxml.DOMDocument",
			"microsoft.XMLDOM"
		);
		
		for (var i = 0; i < msxml2DOMDocumentVersions.length; i++){ //���������� ����� ���������� ������ msxml
			try { //������� ������� ������
				msxml2DOM = new ActiveXObject(msxml2DOMDocumentVersions[i]);
				if (msxml2DOM) return msxml2DOM;
			} catch(e){}
		}

		if (!msxml2DOM){
			alert("Error: old version of MSXML. Update it from 'http://msdn.microsoft.com/XML/XMLDownloads/default.aspx'")
		}
	}
	
	window.xmlHttp = createXmlHttpRequestObject(); //������ �� ������ �������
})();

if (document.getElementsByClassName == undefined) {
	document.getElementsByClassName = function(cl) {
		var retnode = []; 
		var myclass = new RegExp('\\b'+cl+'\\b'); 
		var elem = this.getElementsByTagName('*'); 
		for (var i = 0; i < elem.length; i++) { 
			var classes = elem[i].className; 
			if (myclass.test(classes)) { 
				retnode.push(elem[i]); 
			} 
		}
	 return retnode; 
	}
}

function getChar(event) {
	if (event.which == null) {
		if (event.keyCode < 32) return null;
		return String.fromCharCode(event.keyCode) // IE
	}

	if (event.which!=0 && event.charCode!=0) {
		if (event.which < 32) return null;
		return String.fromCharCode(event.which)	 // ���������
	}

	return null; // ����������� �������
}

function fixEvent(e) {
	e = e || window.event;
	if (!e.target)
		e.target = e.srcElement;
	if (e.pageX == null && e.clientX != null) {
		var html = document.documentElement;
		var body = document.body;
		e.pageX = e.clientX + (html.scrollLeft || body && body.scrollLeft || 0);
		e.pageX -= html.clientLeft || 0;
		e.pageY = e.clientY + (html.scrollTop || body && body.scrollTop || 0);
		e.pageY -= html.clientTop || 0;
	}
	if (!e.which && e.button) {
		e.which = e.button & 1 ? 1 : (e.button & 2 ? 3 : (e.button & 4 ? 2 : 0));
	}
	return e;
}
	
function fixPageXY(e) {
	if (e.pageX == null && e.clientX != null ) { // ���� ��� pageX..
		var html = document.documentElement;
		var body = document.body;

		e.pageX = e.clientX + (html.scrollLeft || body && body.scrollLeft || 0);
		e.pageX -= html.clientLeft || 0;
		
		e.pageY = e.clientY + (html.scrollTop || body && body.scrollTop || 0);
		e.pageY -= html.clientTop || 0;
	}
}

function remove(elem) {
	return elem.parentNode ? elem.parentNode.removeChild(elem) : elem;
}

function getCoords(elem) {
	// (1)
	var box = elem.getBoundingClientRect();
	
	var body = document.body;
	var docEl = document.documentElement;
	
	// (2)
	var scrollTop = window.pageYOffset || docEl.scrollTop || body.scrollTop;
	var scrollLeft = window.pageXOffset || docEl.scrollLeft || body.scrollLeft;
	
	// (3)
	var clientTop = docEl.clientTop || body.clientTop || 0;
	var clientLeft = docEl.clientLeft || body.clientLeft || 0;
	
	// (4)
	var top	= box.top +	scrollTop - clientTop;
	var left = box.left + scrollLeft - clientLeft;
	
	// (5)
	return { top: Math.round(top), left: Math.round(left) };
}

function createCSSSelector(selector, style) {
	if(!document.styleSheets) {
		return;
	}

	if(document.getElementsByTagName("head").length == 0) {
		return;
	}

	var stylesheet;
	var mediaType;
	if(document.styleSheets.length > 0) {
		for( i = 0; i < document.styleSheets.length; i++) {
			if(document.styleSheets[i].disabled) {
				continue;
			}
			var media = document.styleSheets[i].media;
			mediaType = typeof media;

			if(mediaType == "string") {
				if(media == "" || (media.indexOf("screen") != -1)) {
					styleSheet = document.styleSheets[i];
				}
			} else if(mediaType == "object") {
				/*if(media.mediaText == "" || (media.mediaText.indexOf("screen") != -1)) {
					styleSheet = document.styleSheets[i];
				}*/
			}

			if( typeof styleSheet != "undefined") {
				break;
			}
		}
	}

	if( typeof styleSheet == "undefined") {
		var styleSheetElement = document.createElement("style");
		styleSheetElement.type = "text/css";

		document.getElementsByTagName("head")[0].appendChild(styleSheetElement);

		for( i = 0; i < document.styleSheets.length; i++) {
			if(document.styleSheets[i].disabled) {
				continue;
			}
			styleSheet = document.styleSheets[i];
		}

		var media = styleSheet.media;
		mediaType = typeof media;
	}

	if(mediaType == "string") {
		for( i = 0; i < styleSheet.rules.length; i++) {
			if(styleSheet.rules[i].selectorText && styleSheet.rules[i].selectorText.toLowerCase() == selector.toLowerCase()) {
				styleSheet.rules[i].style.cssText = style;
				return;
			}
		}

		styleSheet.addRule(selector, style);
	} else if(mediaType == "object") {
		for( i = 0; i < styleSheet.cssRules.length; i++) {
			if(styleSheet.cssRules[i].selectorText && styleSheet.cssRules[i].selectorText.toLowerCase() == selector.toLowerCase()) {
				styleSheet.cssRules[i].style.cssText = style;
				return;
			}
		}

		styleSheet.insertRule(selector + "{" + style + "}", styleSheet.cssRules.length);
	}
}

function addClass(el, cls) { 
	var c = el.className ? el.className.split(' ') : [];
	for (var i=0; i<c.length; i++) {
		if (c[i] == cls) return;
	}
	c.push(cls);
	el.className = c.join(' ');
}

function removeClass(el, cls) {
	var c = el.className.split(' ');
	for (var i=0; i<c.length; i++) {
		if (c[i] == cls) c.splice(i--, 1);
	}

	el.className = c.join(' ');
}

function hasClass(el, cls) {
	for (var c = el.className.split(' '),i=c.length-1; i>=0; i--) {
		if (c[i] == cls) return true;
	}
	return false;
}

function clientWidth(){ // ������ ���� ���������
	return document.documentElement.clientWidth == 0 ? document.body.clientWidth : document.documentElement.clientWidth;
}

function clientHeight(){ // ������ ���� ���������
	return document.documentElement.clientHeight == 0 ? document.body.clientHeight : document.documentElement.clientHeight;
}
	
function documentWidth(){ // ������ ����� ���������
	return Math.max(
		document.documentElement["clientWidth"],
		document.body["scrollWidth"], document.documentElement["scrollWidth"],
		document.body["offsetWidth"], document.documentElement["offsetWidth"]
	);
}

function documentHeight(){ // ������ ����� ���������
	return Math.max(
		document.documentElement["clientHeight"],
		document.body["scrollHeight"], document.documentElement["scrollHeight"],
		document.body["offsetHeight"], document.documentElement["offsetHeight"]
	);
}

var getPageScroll = (window.pageXOffset != undefined) ?
	function() {
		return {
			left: pageXOffset,
			top: pageYOffset
		};
	} :
	function() {
		var html = document.documentElement;
		var body = document.body;

		var top = html.scrollTop || body && body.scrollTop || 0;
		top -= html.clientTop;

		var left = html.scrollLeft || body && body.scrollLeft || 0;
		left -= html.clientLeft;

		return { top: top, left: left };
	}