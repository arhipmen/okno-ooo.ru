//НАБОР ФУНКЦИЙ
//---------------//

//---------------//
//ВЫВОД ПОДСКАЗОК
(function() {
	function Hint(className, type) {
		var self = this;
		var timeout;
		
		var caller;
		
		if (!type) type = "over";

		this.setHandlers = function(type) {
			var collection = document.getElementsByClassName(className);

			for (var i = 0; i < collection.length; i++) { 
				if (type == "over") {
					collection[i].onmouseover = function(e) {
						e = e || event;

						self.handlersActions(e);
					}
				} else {
					collection[i].onclick = function(e) {
						e = e || event;

						self.handlersActions(e);
					}
				}
				
				collection[i].onmouseout = function(e) {
					if (timeout) clearTimeout(timeout);
					timeout = setTimeout(function() {
						self.clear();
					}, 300);
				}
			}
		}
		
		this.handlersActions = function(e) {
			fixPageXY(e);
			var target = e.target || e.srcElement;
		
			if (timeout) clearTimeout(timeout);

			while (target.className != className || target == document) {
				target = target.parentNode;
			}
			if (target == caller) return;
			caller = target;

			var text = target.getAttribute('data-hint');
			
			if (!text) return;
			
			var x = e.pageX;
			var y = e.pageY;
			
			x = getCoords(target).left;
			y = getCoords(target).top ;

			self.show(text, x, y);
		}
		
		self.setHandlers(type);
		
		this.show = function(text, x, y) {
			var el = document.getElementById(hintEl);
			if (el) remove(el);
			if (timeout) clearTimeout(timeout);

			el = document.createElement("div");
			el.id = "hint";
			el.style.opacity = 1.0;
			el.innerHTML = text +
			'<div style = "clear"both"></div>';
			
			el.onmouseover = function() {
				if (timeout) clearTimeout(timeout);
			}
			el.onmouseout = function() {
				if (timeout) clearTimeout(timeout);
				timeout = setTimeout(function() {
					self.clear();
				}, 300);
			}
			
			var style = Array({
				selector: "#hint",
				css: "display:none;margin-top:70px;width:300px;border:solid 0px #6e7781;border-radius:2px;-moz-border-radius:2px;-webkit-border-radius:2px;text-align:center;border:solid 1px #6e7781;font-size:14px;position:absolute;padding:20px;background-color:#fff;z-index:9;font-size:14px;line-height:20px;-webkit-box-shadow: 0px 0px 27px 0px rgba(24, 29, 33, 0.65);-moz-box-shadow: 0px 0px 27px 0px rgba(24, 29, 33, 0.65);box-shadow: 0px 0px 27px 0px rgba(24, 29, 33, 0.65);"}
			)
			
			for (var i = 0; i < style.length; i++) {
				createCSSSelector(style[i]['selector'], style[i]['css']);
			}

			document.body.appendChild(el);
		
			var elHeight = document.getElementById("hint").offsetHeight;
			var elWidth = document.getElementById("hint").offsetWidth;
			
			var windowY = y - getPageScroll().top;
			var windowX = x - getPageScroll().left;

			if ((windowY + elHeight) > (clientHeight() - 80)) {
				var diffY = (windowY + elHeight) - clientHeight();
				el.style.top = y - diffY - 80 + "px";
			} else {
				el.style.top = y + "px";
			}
			
			if ((windowX + elWidth) > (clientWidth() - 20)) {
				var diffX = (windowX + elWidth) - clientWidth();
				el.style.left = x - diffX - 20 + "px";
			} else {
				el.style.left = x + "px";
			}

			$(document).ready(function() {
				$("#hint").animate({opacity: 'show', marginTop:"80px"}, 500); 
			});
		}
		
		this.clear = function() {
			if (timeout) clearTimeout(timeout);
			var el = document.getElementById(hintEl);
			if (!el) return;
			if (window.jQuery) {
				$(el).fadeOut("slow", function() {remove(el)});
			} else {
				el.style.display = "none";
				remove(el);
			}
			caller = null;
		}
	}
	
	var hintEl = "hint"; //id элемента с подсказкой

	window.hint2 = new Hint("hinted", "over");
})();

//МОДАЛЬНОЕ ОКНО
(function() {
	window.ModalForm = function() {
		var self = this;
		var el = document.createElement("img");
		el.src = "/_res/img/close.png";
		
		this.showModal = function(text) {
			if (document.getElementById("modalMessage")) self.closeModal();
			var el = document.createElement("div");
			el.id = "modalMessage";
			el.innerHTML = '<div style = "position: fixed;left: 0;top: 0;width: 100%;height: 100%;z-index: 999;">' +
				'<div id = "fader"></div>' + 
				'<div id = "modalCarrier">' +
				'<div id = "modal">' +
				'<div style = "position:relative;float:right;top:0px;right:0px;"><img src = "/_res/img/close.png" style = "position:absolute;float:right;top:0px;right:0px;cursor:pointer;margin:-7px -65px 0px 0px" onclick = "modalForm.closeModal()" alt = "X"></div>'+
				text +
			'</div></div></div>';
			
			var style = Array({
					selector: "#fader",
					css: "position: absolute;left: 0px;right: 0px;top: 0px;bottom: 0px;background: #666;opacity: 0;filter: alpha(opacity=0);"},
				{
					selector: "#modalCarrier",
					css: "position: absolute;top: 36%; right: 0; left: 0;z-index:999;text-align:center;"},
				{
					selector: "#modalCarrier #modal",
					css: "margin: auto;display:inline-block;border: 1px solid #d5dadd;padding:15px 75px 15px 75px;background-color:#fff;opacity: 1.0;filter: alpha(opacity=100);text-align:left;z-index:999;border-radius:5px;-moz-border-radius:5px;-webkit-border-radius:5px;"},
				{
					selector: "#modalCarrier #modal label",
					css: "font-size:15px;padding:7px 10px 0px 0px;display:block;"
				},
				{
					selector: "#modalCarrier #modal input",
					css: "width:160px;"
				}
			)
			
			for (var i = 0; i < style.length; i++) {
				createCSSSelector(style[i]['selector'], style[i]['css']);
			}
			
			document.body.appendChild(el);
			
			$(document).ready(function() {
				$("#fader").animate({
					opacity: 0.7,
				}, 500 );
			});
		}
		
		this.closeModal = function() {
			var el = document.getElementById("modalMessage");
			if (el) remove(el);
		}
		
		function remove(elem) {
			return elem.parentNode ? elem.parentNode.removeChild(elem) : elem;
		}
	}
	
	window.modalForm = new ModalForm();
})();

//---------------//
//ВЫВОД модальных ИНФОРМАЦИОННЫХ СООБЩЕНИЙ
(function() {
	function Message() {//объект сообщений
		var self = this;
		var timeout; //обратный отсчет

		this.show = function(message, isError) { //показать сообщение
			if (timeout) clearTimeout(timeout);
			timeout = setTimeout(function() {
				self.clear();
			}, 10000);

			if (document.getElementById("userMessage")) { //если уже есть сообщение
				remove(document.getElementById("userMessage")); //может быть только одно сообщение на странице
			}

			var messageEl = document.createElement("div");//создаем новое
			
			messageEl.id = "userMessage";
			
			var style = Array({
					selector: "#userMessage",
					css: "position:fixed;z-index:999;padding:20px;width:300px;top:45%;left:0px;margin-left:-300px;background-color: rgba(176, 59, 128, 1.0);color:#fff;-webkit-box-shadow: 0px 1px 5px 0px rgba(0, 0, 0, 0.15);-moz-box-shadow: 0px 1px 5px 0px rgba(0, 0, 0, 0.15);box-shadow:  0px 1px 5px 0px rgba(0, 0, 0, 0.15);-webkit-border-radius:0px 5px 5px 0px;-moz-border-radius:0px 5px 5px 0px;border-radius:0px 5px 5px 0px;border:solid 0px #b03b80;text-align:left;"}
			);
			
			for (var i = 0; i < style.length; i++) {
				createCSSSelector(style[i]['selector'], style[i]['css']);
			}

			messageEl.innerHTML = "<div style = 'opacity:1.0;'>" + message + "</div>";
			
			document.body.appendChild(messageEl);

			//$(messageEl).slideToggle(400);
			//$(messageEl).slideToggle(400);
			$(messageEl).animate({
				marginLeft: '0px'
			}, 400 );

			messageEl.onmouseover = function() {
				self.clear();
			}
		}
		
		this.clear = function() { //очистка сообщений
			if (timeout) clearTimeout(timeout);
			if (!document.getElementById("userMessage")) return;

			var messageEl = document.getElementById("userMessage");
			
		//	$(messageEl).slideToggle(200, function(){remove(messageEl)});
			$(messageEl).animate({
				marginLeft: '-300px'
			}, 400, "linear", function(){remove(messageEl)});
			
		}
		
		function remove(elem) {
			return elem.parentNode ? elem.parentNode.removeChild(elem) : elem;
		}
	}
	
	window.message = new Message();
})();

//---------------//
//ОТПРАВКА СООБЩЕНИЙ
(function() {
	function Send() { //объект отправки
		var self = this;
		var silent;
		var sendXmlHttp = createXmlHttpRequestObject();

		this.server = "/wp-content/themes/vsesoknami_theme/sendMail.php";
		
		this.query = function(target, fieldsInfo, additionFields, s) {
			silent = s;
			var data = additionFields;
			var el = target;

			while (el.tagName != "FORM") { //ищем контейнер
				var el = el.parentNode;
				if (el == document) return;
			}
			var tags = Array('input', 'textarea', 'select');
				
			for (var i = 0; i < tags.length; i++) {
				var els = el.getElementsByTagName(tags[i]);
				for (var j = 0; j < els.length; j++) {
					if (data[els[j].getAttribute('data-id')]) {
						alert('Ошибка. Поле data-id не уникально');
						return;
					}
					
					if (els[j].getAttribute('type') == "checkbox") {
						if (els[j].checked) {
							els[j].value = 1;
						} else {
							els[j].value = 0;
						}
					}
					
					//alert(els[j].getAttribute('data-id'));
					if (errorCheck(els[j].getAttribute('data-id'), els[j].value)) {
						return;
					}

					data[els[j].getAttribute('data-id')] = els[j].value;
				}
			}

			data["action"] = target.getAttribute('data-action');
			data["yandexId"] = target.getAttribute('data-form-id');
			data["locus"] = window.location.href;

		//	var query = encodeURIComponent("query=" + JSON.stringify(data));
			var query = "query=" + JSON.stringify(data);
			
		//	alert(query);

			function errorCheck(id, value) {
				if (!fieldsInfo[id]) return false;

				if (fieldsInfo[id]["required"]) {
					if (value.length < fieldsInfo[id]["minLength"] || value == fieldsInfo[id]["defaultVal"]) {
						message.show(fieldsInfo[id]["error"], 1);
						return true;
					}
				}
				
				if (value == fieldsInfo[id]["defaultVal"]) els[j].value = "";
			}
			
			self.post(query);
			
			return true;
		}
		
		this.post = function(query) {
			var self = this;
			if (sendXmlHttp && (sendXmlHttp.readyState == 4 || sendXmlHttp.readyState == 0)) {
				sendXmlHttp.open("post", this.server, true);
				sendXmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
				sendXmlHttp.setRequestHeader("Content-length", query.length);
				sendXmlHttp.onreadystatechange = function() {
					var readyEl = document.getElementById("state" + sendXmlHttp.readyState);
					if (sendXmlHttp.readyState == 4) {
						if (sendXmlHttp.status == 200) {
							self.commonHandler();
						} else {
							message.show(" Сервер не отвечает, пожалуйста, воспользуйтесь другим способом связи с нами");
							throw new Error("Error: Status" + sendXmlHttp.status);
						}
					}
				};

				sendXmlHttp.send(query);
			}
		}
			
		this.commonHandler = function() {
			if (silent) return;
			var textResponse = sendXmlHttp.responseText;
			//alert(textResponse);
			if (textResponse.length > 0) {
				if (window.message) {
					message.show(textResponse, 0);
					modalForm.closeModal();
				} else {
					alert("No message Object");
				}

				return;
			}
			if (window.message) message.show(" Сервер не отвечает, пожалуйста, воспользуйтесь другим способом связи с нами.", 1);
		}
	}
	
	window.send = new Send();
	window.send2 = new Send();
})();

//ЗАПРЕТ НА ВВОД ВСЕГО, КРОМЕ ЦИФР
function onlyNums(arr) {
	for (var i = 0; i < arr.length; i++) {
		arr[i].onkeypress = function(e) {
			e = e || event;
			
			if (e.ctrlKey || e.altKey || e.metaKey) return;

			var chr = getChar(e);

			if (chr == null) return;

			if (chr < '0' || chr > '9') {
				return false;
			}
			
		}
		arr[i].onkeydown = function(e) { //отмена backspace для удаления +7
			e = e || event;
			var key = e.keyCode || e.which;
			if (key == 8) {
				if (this.value == "+7") return false;
			}
		}
	}
}

//ЗАПРЕТ НА ВВОД ВСЕГО, КРОМЕ БУКВ
function onlyWords(arr) {
	for (var i = 0; i < arr.length; i++) {
		arr[i].onkeypress = function(e) {
			e = e || event;

			if (e.ctrlKey || e.altKey || e.metaKey) return;

			var chr = getChar(e);

			if (chr == null) return;
			
			if (chr >= '0' && chr <= '9') return false;
		}
	}
}