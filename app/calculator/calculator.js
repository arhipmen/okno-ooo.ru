//---------------//
//КАЛЬКУЛЯТОР

(function() {
	$('[name="window"]').val('1-1');
	var currentProfile = "blitz";
	var currentLeaf = "leaf1";
	var leafNum = 0;

	var leafs2 = {
		dver: {dver: "calculator/leafs/door/dver.png"}, 
		leaf1: {
			leaf1_1: "calculator/leafs/leaf1/1_stvorka_1.png", 
			leaf1_2: "calculator/leafs/leaf1/1_stvorka_2.png", 
			leaf1_3: "calculator/leafs/leaf1/1_stvorka_3.png"
		},
		leaf2: {
			leaf2_1: "calculator/leafs/leaf2/2_stvorka_1.png", 
			leaf2_2: "calculator/leafs/leaf2/2_stvorka_2.png", 
			leaf2_3: "calculator/leafs/leaf2/2_stvorka_3.png", 
			leaf2_4: "calculator/leafs/leaf2/2_stvorka_4.png"
		},
		leaf3 : {
			leaf3_1: "calculator/leafs/leaf3/3_stvorka_1.png", 
			leaf3_2: "calculator/leafs/leaf3/3_stvorka_2.png", 
			leaf3_3: "calculator/leafs/leaf3/3_stvorka_3.png",
			leaf3_4: "calculator/leafs/leaf3/3_stvorka_4.png"
		}
	}

	var data = {
		blitz: {
			name: "Blitz",
			image: "img/1.jpg",
			leafs: {
				dver: Array(
					{
						average: 6100
					}), 
				leaf1: Array(
					{
						average: 3924
					}, 
					{
						average: 7029
					}, 
					{
						average: 7512
					}),
				leaf2: Array(
					{
						average: 4905
					}, 
					{
						average: 5577
					},
					{
						average: 6405
					},  
					{
						average: 5808
					}),
				leaf3: Array(
					{
						average: 5808
					}, 
					{
						average: 5968
					},  
					{
						average: 4772
					},
					{
						average: 4933
					})
			}
		},
		grazio: {
			name: "Grazio",
			image: "img/2.jpg",
			leafs: {
				dver: Array(
					{
						average: 6420
					}), 
				leaf1: Array(
					{
						average: 4099
					}, 
					{
						average: 7435
					}, 
					{
						average: 7918
					}),
				leaf2: Array(
					{
						average: 5150
					}, 
					{
						average: 5814
					},  
					{
						average: 6820
					},
					{
						average: 7020
					}),
				leaf3: Array(
					{
						average: 6064
					}, 
					{
						average: 6226
					},  
					{
						average: 4953
					},
					{
						average: 5114
					})
			}
		},
		delight: {
			name: "Delight",
			image: "img/4.jpg",
			leafs: {
				dver: Array(
					{
						average: 7302
					}), 
				leaf1: Array(
					{
						average: 4667
					}, 
					{
						average: 8374
					}, 
					{
						average: 8856
					},
					{
						average: 5980
					}),
				leaf2: Array(
					{
						average: 6488
					}, 
					{
						average: 7015
					},  
					{
						average: 7950
					}),
				leaf3: Array(
					{
						average: 6773
					}, 
					{
						average: 6934
					},  
					{
						average: 5538
					},
					{
						average: 5698
					})
			}
		},
		brilliant: {
			name: "Brilliant",
			image: "img/6.jpg",
			leafs: {
				dver: Array(
					{
						average: 8170
					}), 
				leaf1: Array(
					{
						average: 5127
					}, 
					{
						average: 9300
					}, 
					{
						average: 9790
					}),
				leaf2: Array(
					{
						average: 6350
					}, 
					{
						average: 6890 
					}, 
					{
						average: 8960 
					},
					{
						average: 9201
					}),
				leaf3: Array(
					{
						average: 7002
					}, 
					{
						average: 7422
					},  
					{
						average: 6028
					},
					{
						average: 6190
					})
			}
		},
		intelio: {
			name: "Intelio",
			image: "img/3.jpg",
			leafs: {
				dver: Array(
					{
						average: 8170
					}), 
				leaf1: Array(
					{
						average: 5127
					}, 
					{
						average: 9300
					}, 
					{
						average: 9790
					}),
				leaf2: Array(
					{
						average: 6350
					}, 
					{
						average: 6890 
					}, 
					{
						average: 8960 
					},
					{
						average: 9201
					}),
				leaf3: Array(
					{
						average: 7002
					}, 
					{
						average: 7422
					},  
					{
						average: 6028
					},
					{
						average: 6190
					})
			}
		},
		tandem: {
			name: "Tandem",
			image: "img/5.jpg",
			leafs: {
				dver: Array(
					{
						average: 8170
					}), 
				leaf1: Array(
					{
						average: 5127
					}, 
					{
						average: 9300
					}, 
					{
						average: 9790
					}),
				leaf2: Array(
					{
						average: 6350
					}, 
					{
						average: 6890 
					}, 
					{
						average: 8960 
					},
					{
						average: 9201
					}),
				leaf3: Array(
					{
						average: 7002
					}, 
					{
						average: 7422
					},  
					{
						average: 6028
					},
					{
						average: 6190
					})
			}
		},
		geneo: {
			name: "Geneo",
			image: "img/7.jpg",
			leafs: {
				dver: Array(
					{
						average: 8170
					}), 
				leaf1: Array(
					{
						average: 5127
					}, 
					{
						average: 9300
					}, 
					{
						average: 9790
					}),
				leaf2: Array(
					{
						average: 6350
					}, 
					{
						average: 6890 
					}, 
					{
						average: 8960 
					},
					{
						average: 9201
					}),
				leaf3: Array(
					{
						average: 7002
					}, 
					{
						average: 7422
					},  
					{
						average: 6028
					},
					{
						average: 6190
					})
			}
		}
	}
	
	var heightBoxCarrier = document.getElementById("verticalScroll");
	var widthBoxCarrier = document.getElementById("horizontalScroll");
	
	var heightBox = document.getElementById("verticalBox");
	var widthBox = document.getElementById("horizontalBox");
	
	var heightInput = document.getElementById("windowHeight");
	var widthInput = document.getElementById("windowWidth");
	
	var currentProfileEl = document.getElementById("currentImage");
	
	var totalPriceEl = document.getElementById("totalPrice");
/*
	$("#modelMenu").on("hover", function() {
		$("#modelList").animate({display: 'block'}, 500); 
	});*/
	$(document).ready(function() {
		$("#modelMenu").hover(function() {
			$("#modelList").stop().show(300);
		}, function() {
			$("#modelList").stop().hide(200);
		});
    });
	
//обработчик на профили
	var els = document.getElementsByClassName("profile");
	for (var i = 0; i < els.length; i++) {
		els[i].onclick = function(e) {
			e = e || event;
			var target = e.target || e.srcElement;
			
			var els = document.getElementsByClassName("profile");
			
			for (var i = 0; i < els.length; i++) {
				els[i].style.background = "";
				els[i].style.color = "#fff";
			}
			
			target.style.color = "#000";
			currentProfile = target.getAttribute("data-profile");

			document.getElementById("profileTitle").innerHTML = data[currentProfile].name;

			currentProfileEl.src = data[currentProfile].image;

			setPrice();
			
			$(document).ready(function() {
				$('html, body').stop().animate({scrollLeft: 0, scrollTop:$("#calculatorBlock").offset().top - 50}, 500);
			});

		}
	}

//обработчик на раскладки
	var els = document.getElementsByClassName("leafsMenu");
	for (var i = 0; i < els.length; i++) {
		els[i].onclick = function(e) {
			e = e || event;
			var target = e.target || e.srcElement;
			
			currentLeaf = target.getAttribute("data-leaf");
			
			var els = document.getElementsByClassName("leafsMenu");
			for (var i = 0; i < els.length; i++) {
				els[i].style.borderColor = "#fff";
			}
			
		//	leafImage.src = leafs[currentLeaf][leafNum];
	
			target.style.borderColor = "#9f005e";
			
			leafNum = target.getAttribute("data-leaf-num");;

		//	checkLeafs();
			setImage(this);
			setPrice();
			
			$(document).ready(function() {
				$('html, body').stop().animate({scrollLeft: 0, scrollTop:$("#calculatorBlock").offset().top - 50}, 500);
			});
		}
	}

	function setImage(el) {
		var src = el.src;
		var nEl = el.parentNode.parentNode.parentNode.parentNode;
		nEl.firstChild.src = src;
		nEl.firstChild.style.border = "solid 2px #9f005e";
		var arr = src.split("/");
		var image = document.getElementById("currentLeaf");
		image.src = "calculator/leafs/big/" + arr[arr.length - 2] + "/" + arr[arr.length - 1];

		switch(arr[arr.length - 1]) {
			case '1_stvorka_1.png': $('[name="window"]').val('1-1'); break;
			case '1_stvorka_2.png': $('[name="window"]').val('1-2'); break;
			case '1_stvorka_3.png': $('[name="window"]').val('1-3'); break;
			case '2_stvorka_1.png': $('[name="window"]').val('2-1'); break;
			case '2_stvorka_2.png': $('[name="window"]').val('2-2'); break;
			case '2_stvorka_3.png': $('[name="window"]').val('2-3'); break;
			case '2_stvorka_4.png': $('[name="window"]').val('2-4'); break;
			case '3_stvorka_1.png': $('[name="window"]').val('3-1'); break;
			case '3_stvorka_2.png': $('[name="window"]').val('3-2'); break;
			case '3_stvorka_3.png': $('[name="window"]').val('3-3'); break;
			case '3_stvorka_4.png': $('[name="window"]').val('3-4'); break;
		}
	}

	var priceArr = document.getElementsByClassName('leaf');
	for (var i = 0; i < priceArr.length; i++) {
		priceArr[i].onmouseover = function (e) {
			e = e || window.event;
			var divArr = this.getElementsByClassName("drop");
			if (divArr)
				divArr[0].style.display = "block";
		}
		priceArr[i].onmouseout = function (e) {
			e = e || window.event;
			var divArr = this.getElementsByClassName("drop");
			if (divArr)
				divArr[0].style.display = "none";
		}
	}

	document.getElementById("windowHeight").onchange = document.getElementById("windowWidth").onchange = function () {
		setPrice();
	}

		
	heightBox.ondragstart = widthBox.ondragstart = function () {
		return false;
	}
		
	heightBox.onmousedown = function(e) {
		e = fixEvent(e);
		var target = e.target || e.srcElement;
	
		var coords = getCoords(target);
		var shiftX = e.pageX - coords.left;
		var shiftY = e.pageY - coords.top;
		var sliderCoords = getCoords(target.parentNode);
		
		var parent = target.parentNode;

		if (target.id == "horizontalBox") {
			var el = widthBox;
		} else {
			var el = heightBox;
		}
			
		document.onmousemove = function(e) {
			e = fixEvent(e);
			var target = e.target || e.srcElement;

			var newTop = e.pageY - shiftY - sliderCoords.top;
			if (newTop < 0) {
				newTop = 0;
			}
			var topEdge = parent.offsetHeight - el.offsetHeight;
			if (newTop > topEdge) {
				newTop = topEdge;
			}
			el.style.top = newTop + 'px';

			setHeight(newTop);
			
			setPrice();
		}

		document.onmouseup = function () {
			document.onmousemove = document.onmouseup = null;
		}

		return false;
	}
	
	widthBox.onmousedown = function(e) {
		e = fixEvent(e);
		var target = e.target || e.srcElement;
	
		var coords = getCoords(target);
		var shiftX = e.pageX - coords.left;
		var shiftY = e.pageY - coords.top;
		var sliderCoords = getCoords(target.parentNode);
		
		var parent = target.parentNode;

		if (target.id == "horizontalBox") {
			var el = widthBox;
		} else {
			var el = heightBox;
		}
			
		document.onmousemove = function(e) {
			e = fixEvent(e);
			var target = e.target || e.srcElement;

			var newLeft = e.pageX - shiftX - sliderCoords.left;
			if (newLeft < 0) {
				newLeft = 0;
			}
			var rightEdge = parent.offsetWidth - el.offsetWidth;
			if (newLeft > rightEdge) {
				newLeft = rightEdge;
			}
			el.style.left = newLeft + 'px';

			setWidth(newLeft);

			setPrice();
		}

		document.onmouseup = function () {
			document.onmousemove = document.onmouseup = null;
		}

		return false;
	}
	
	heightBoxCarrier.onclick = function(e) {
		e = fixEvent(e);
		var target = e.target || e.srcElement;
		
		var coords = getCoords(this);
		var top = e.pageY - coords.top;

		setHeight(top);
		
		heightBox.style.top = top - 7 + "px";

		setPrice();
	}
	
	widthBoxCarrier.onclick = function(e) {
		e = fixEvent(e);
		var target = e.target || e.srcElement;
		
		var coords = getCoords(this);
		var left = e.pageX - coords.left;

		setWidth(left);
		widthBox.style.left = left - 7 + "px";
		
		setPrice();
	}

	function setWidth(pos) {
		var width = 500 + pos * 20;
		widthInput.value = width;
	}
	
	function setHeight(pos) {
		var height = 500 + (230 - pos) * 18;
		heightInput.value = height;
	}
	
	function setPrice() {
		var s = heightInput.value * widthInput.value / 1000000;

		if (data[currentProfile]['leafs'][currentLeaf][leafNum]['formula']){
			var price = s * (data[currentProfile]['leafs'][currentLeaf][leafNum]['formula'])(s);
		} else {
			var price = data[currentProfile]['leafs'][currentLeaf][leafNum]['average'] * s;
		}
		
		
		totalPriceEl.innerHTML = Math.ceil(price) + " р.";
	}

	function fixEvent(e) {
		e = e || window.event;
		if (!e.target)
			e.target = e.srcElement;
		if (e.pageX == null && e.clientX != null) {
			var html = document.documentElement;
			var body = document.body;
			e.pageX = e.clientX + (html.scrollLeft || body && body.scrollLeft || 0);
			e.pageX -= html.clientLeft || 0;
			e.pageY = e.clientY + (html.scrollTop || body && body.scrollTop || 0);
			e.pageY -= html.clientTop || 0;
		}
		if (!e.which && e.button) {
			e.which = e.button & 1 ? 1 : (e.button & 2 ? 3 : (e.button & 4 ? 2 : 0));
		}
		return e;
	}

	function getCoords(elem) {
		var box = elem.getBoundingClientRect();
		var body = document.body;
		var docElem = document.documentElement;
		var scrollTop = window.pageYOffset || docElem.scrollTop || body.scrollTop;
		var scrollLeft = window.pageXOffset || docElem.scrollLeft || body.scrollLeft;
		var clientTop = docElem.clientTop || body.clientTop || 0;
		var clientLeft = docElem.clientLeft || body.clientLeft || 0;
		var top = box.top + scrollTop - clientTop;
		var left = box.left + scrollLeft - clientLeft;
		return {
			top : Math.round(top),
			left : Math.round(left)
		};
	}
	
	setPrice();
})();